package com.example.demo;

import com.example.demo.controller.MainController;
import com.example.demo.service.EncryptService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WebMvcTest(MainController.class)
public class DemoApplicationTests {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private EncryptService service;

	@Test
	public void contextLoads() {
	}

}
