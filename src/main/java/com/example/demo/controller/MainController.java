package com.example.demo.controller;


import com.example.demo.service.EncryptService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Main controller.
 */
@RestController
public class MainController {

	private static final Logger logger = LogManager.getLogger(MainController.class);

	private final EncryptService encryptService;

	/**
	 * Instantiates a new Main controller.
	 *
	 * @param encryptService the encrypt service
	 */
	@Autowired
	public MainController(EncryptService encryptService) {
		this.encryptService = encryptService;
	}


	/**
	 * Encrypt response entity.
	 *
	 * @param encryptedString the encrypted string
	 * @param secretKey       the secret key
	 * @return the response entity
	 */
	@RequestMapping("/encrypt")
	public ResponseEntity<String> encrypt(@RequestParam(value = "encryptedString") String encryptedString,
										  @Value("${my.secret.key}") String secretKey) {
		if (encryptedString.isEmpty()) {
			return new ResponseEntity<>("Bad request", HttpStatus.BAD_REQUEST);
		}
		String decryptedString = encryptService.encrypt(encryptedString);
		if (decryptedString.isEmpty()) {
			return new ResponseEntity<>("No content decrypted string", HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(secretKey + " - " + encryptedString + " - " + decryptedString, HttpStatus.OK);
	}

	/**
	 * Decrypt response entity.
	 *
	 * @param decryptedString the decrypted string
	 * @param secretKey       the secret key
	 * @return the response entity
	 */
	@RequestMapping("/decrypt")
	public ResponseEntity<String> decrypt(@RequestParam(value = "decryptedString") String decryptedString,
										  @Value("${my.secret.key}") String secretKey) {
		if (decryptedString.isEmpty()) {
			return new ResponseEntity<>("Bad request", HttpStatus.BAD_REQUEST);
		}
		String encryptedString = encryptService.decrypt(decryptedString);
		if (encryptedString.isEmpty()) {
			return new ResponseEntity<>("No content encrypted string", HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(secretKey + " - " + decryptedString + " - " + encryptedString, HttpStatus.OK);
	}
}
