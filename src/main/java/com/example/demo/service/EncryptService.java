package com.example.demo.service;

public interface EncryptService {

	String encrypt(String toEncrypt);

	String decrypt(String encrypted);
}
