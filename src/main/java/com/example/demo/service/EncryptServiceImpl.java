package com.example.demo.service;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

/**
 * The type Encrypt service.
 */
@Service
public class EncryptServiceImpl implements EncryptService {

	private static final Logger logger = LogManager.getLogger(EncryptService.class);

	private IvParameterSpec ivParameterSpec;
	private SecretKeySpec secretKeySpec;
	private Cipher cipher;

	/**
	 * Instantiates a new Encrypt service.
	 *
	 * @param secretKey the secret key
	 * @throws Exception the exception
	 */
	public EncryptServiceImpl(@Value("${my.secret.key}") String secretKey) throws Exception {
		byte[] ivParameterByte = new byte[16];
		new SecureRandom().nextBytes(ivParameterByte);
		ivParameterSpec = new IvParameterSpec(ivParameterByte);
		secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "AES");
		cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	}


	/**
	 * Encrypt the string with this AES algorithm.
	 *
	 * @param toEncrypt string to be encrypt.
	 * @return returns encrypted string.
	 * @throws Exception
	 */
	@Override
	public String encrypt(String toEncrypt) {
		String encryptedString = StringUtils.EMPTY;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
			byte[] encryptedBytes = cipher.doFinal(toEncrypt.getBytes(StandardCharsets.UTF_8));
			encryptedString = encryptedBytes != null ? Base64.encodeBase64String(encryptedBytes) : StringUtils.EMPTY;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return encryptedString;
	}

	/**
	 * Decrypt the string with this AES algorithm.
	 *
	 * @param encrypted string to be encrypt.
	 * @return returns decrypted string.
	 * @throws Exception
	 */
	@Override
	public String decrypt(String encrypted) {
		String decryptString = StringUtils.EMPTY;
		try {
			cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
			byte[] decryptedBytes = cipher.doFinal(Base64.decodeBase64(encrypted));
			decryptString = decryptedBytes != null ? new String(decryptedBytes) : StringUtils.EMPTY;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return decryptString;
	}
}
